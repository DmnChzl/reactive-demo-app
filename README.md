# Reactive Demo Application ⚛️

> _Built With **OpenJDK 17**_

I made reactive **REST** APIs for secure user management (Basic Auth + JWT), with these technologies:

- **Spring Boot**: Spring makes programming Java quicker, easier, and safer for everybody...
  - [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)

- **Project Reactor**: Create Efficient Reactive Systems
  - [https://projectreactor.io/](https://projectreactor.io/)

### Context

- [x] CRUD
- [x] Basic Auth
- [x] JWT

## Process

Clone the project:

```
git clone https://gitlab.com/DmnChzl/Reactive-Demo-App.git
```

### Note(s)

_This application depends on a NoSQL database. Install MongoDB and run the following commands in a new terminal before launching the project:_

```
mkdir DataBase
mongo --dbpath DataBase
```

_This application is based on the `resources/application.yml` file. Please, check it and override the following properties before launching the project:_

```yml
server:
  port: 8585

spring:
  data:
    mongodb:
      host: "localhost"
      port: 27017
      database: "demo"
      
jwt:
  secret: <openssl rand 64 | base64>
  expirationtime: 5 * 60 * 60 * 1000
```

## Requests

**READ** all users:

```
curl -X GET http://localhost:8585/api/users
```

**CREATE** new user:

```
curl -H "Content-Type: application/json" -X POST -d '{"firstName": "John", "lastName": "Doe", "birthDate": "1993-05-21", "email": "john.doe@pm.me", "password": "azerty"}' http://localhost:8585/api/users
```

<details>
  <summary>Authenticate</summary>

  ```
  curl -H "Content-Type: application/json" -X POST -d '{"username": "john.doe@pm.me", "password": "azerty"}' http://localhost:8585/api/auth
  ```
</details>

**READ** user:

```
curl -H "Authorization: Bearer {token}" -X GET http://localhost:8585/api/users/{id}
```

**UPDATE** user:

```
curl -H "Authorization: Bearer {token}" -H "Content-Type: application/json" -X PUT -d '{"firstName": "Jane", "lastName": "Doe", "birthDate": "1991-07-27"}' http://localhost:8585/api/users/{id}
```

**DELETE** user:

```
curl -H "Authorization: Bearer {token}" -X DELETE http://localhost:8585/api/users/{id}
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```