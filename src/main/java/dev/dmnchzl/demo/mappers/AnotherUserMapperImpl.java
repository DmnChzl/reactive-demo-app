package dev.dmnchzl.demo.mappers;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserRequest;

public class AnotherUserMapperImpl implements AnotherUserMapper {

    private static AnotherUserMapperImpl instance;

    private AnotherUserMapperImpl() {}

    public static AnotherUserMapperImpl getInstance() {
        if (instance == null) {
            instance = new AnotherUserMapperImpl();
        }

        return instance;
    }

    @Override
    public User toUserFromRequest(UserRequest userRequest) {
        if (userRequest == null) {
            return null;
        }

        User user = new User();

        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setBirthDate(userRequest.getBirthDate());
        user.setEmail(userRequest.getEmail());
        user.setPassword(userRequest.getPassword());

        return user;
    }

}
