package dev.dmnchzl.demo.mappers;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserRequest;

public interface AnotherUserMapper {

    User toUserFromRequest(UserRequest userRequest);

}
