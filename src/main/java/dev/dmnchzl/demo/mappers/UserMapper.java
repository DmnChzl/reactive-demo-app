package dev.dmnchzl.demo.mappers;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.Period;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(source = ".", target = "fullName", qualifiedByName = "concatName")
    @Mapping(source = ".", target = "yearOld", qualifiedByName = "getYears")
    UserResponse fromUserToResponse(User user);

    @Named("concatName")
    default String concatName(User user) {
        return user.getFirstName() + " " + user.getLastName();
    }

    @Named("getYears")
    default int getYears(User user) {
        return Period.between(user.getBirthDate(), LocalDate.now()).getYears();
    }

    UserDetailsResponse fromUserToDetails(User user);
}
