package dev.dmnchzl.demo.services;

import dev.dmnchzl.demo.mappers.UserMapper;
import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserRequest;
import dev.dmnchzl.demo.model.UserResponse;
import dev.dmnchzl.demo.repositories.UserRepository;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
public class UserServiceImpl implements UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {
        User defaultAdmin = new User.UserBuilder()
            .withFirstName("Test")
            .withLastName("Pilot")
            .withBirthDate(LocalDate.of(1993, 5, 21))
            .withEmail("test.pilot@pm.me")
            .withPassword(passwordEncoder.encode("azerty"))
            .withAdminRole()
            .build();

        userRepository.findByEmail(defaultAdmin.getEmail())
            .switchIfEmpty(userRepository.save(defaultAdmin)).subscribe();

        User defaultUser = new User.UserBuilder()
                .withFirstName("John")
                .withLastName("Doe")
                .withBirthDate(LocalDate.of(1993, 5, 21))
                .withEmail("john.doe@pm.me")
                .withPassword(passwordEncoder.encode("qwerty"))
                .withAdminRole()
                .build();

        userRepository.findByEmail(defaultUser.getEmail())
                .switchIfEmpty(userRepository.save(defaultUser)).subscribe();
    }

    @Override
    public Mono<UserResponse> create(UserRequest userRequest) {
        User user = new User.UserBuilder()
            .withFirstName(userRequest.getFirstName())
            .withLastName(userRequest.getLastName())
            .withBirthDate(userRequest.getBirthDate())
            .withEmail(userRequest.getEmail())
            .withPassword(passwordEncoder.encode(userRequest.getPassword()))
            .withUserRole()
            .build();

        LOGGER.debug(user.toString());

        return userRepository.save(user).map(UserMapper.MAPPER::fromUserToResponse);
    }

    @Override
    public Flux<UserResponse> findAll() {
        return userRepository.findAll().map(UserMapper.MAPPER::fromUserToResponse);
    }

    @Override
    public Mono<UserDetailsResponse> findById(String id) {
        return userRepository.findById(id).map(UserMapper.MAPPER::fromUserToDetails);
    }

    @Override
    public Mono<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Mono<UserResponse> updateById(String id, UserRequest userRequest) {
        return userRepository.findById(id).flatMap(it -> {
            LOGGER.debug(userRequest.toString());

            if (userRequest.getFirstName() != null) {
                it.setFirstName(userRequest.getFirstName());
            }

            if (userRequest.getLastName() != null) {
                it.setLastName(userRequest.getLastName());
            }

            if (userRequest.getBirthDate() != null) {
                it.setBirthDate(userRequest.getBirthDate());
            }

            return userRepository.save(it)
                .map(UserMapper.MAPPER::fromUserToResponse);
        });
    }

    @Override
    public Mono<UserResponse> deleteById(String id) {
        return userRepository.findById(id)
            .map(UserMapper.MAPPER::fromUserToResponse)
            .flatMap(it -> userRepository.deleteById(id)
                .then(Mono.just(it)));
    }

}
