package dev.dmnchzl.demo.services;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserRequest;
import dev.dmnchzl.demo.model.UserResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<UserResponse> create(UserRequest userRequest);

    Flux<UserResponse> findAll();

    Mono<UserDetailsResponse> findById(String id);

    Mono<User> findByEmail(String email);

    Mono<UserResponse> updateById(String id, UserRequest userRequest);

    Mono<UserResponse> deleteById(String id);

}
