package dev.dmnchzl.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private String id;
    private String fullName;
    private String email;
    private int yearOld;

    @Override
    public String toString() {
        return "UserResponse { fullName: '" + this.fullName +
            "', email: '" + this.email +
            "', yearOld: '" + this.yearOld + "' }";
    }

}
