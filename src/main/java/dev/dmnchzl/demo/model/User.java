package dev.dmnchzl.demo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document(collection = "users")
public class User {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;

    @Indexed(unique = true)
    private String email;
    private String password;
    private List<String> roles = new ArrayList<>();

    public User() {}

    public User(String firstName, String lastName, LocalDate birthDate, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.password = password;
    }

    private User(UserBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.birthDate = builder.birthDate;
        this.email = builder.email;
        this.password = builder.password;
        this.roles = builder.roles;
    }

    @Override
    public String toString() {
        return "User { firstName: '" + this.firstName +
            "', lastName: '" + this.lastName +
            "', birthDate: '" + this.birthDate +
            "', email: '" + this.email +
            "', password: '" + this.password + "' }";
    }

    public static class UserBuilder {

        private String firstName;
        private String lastName;
        private LocalDate birthDate;
        private String email;
        private String password;
        private final List<String> roles = new ArrayList<>();

        public UserBuilder() {}

        public UserBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public UserBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder withAdminRole() {
            this.roles.add("ROLE_ADMIN");
            return this;
        }

        public UserBuilder withUserRole() {
            this.roles.add("ROLE_USER");
            return this;
        }

        public User build() {
            return new User(this);
        }

    }

}
