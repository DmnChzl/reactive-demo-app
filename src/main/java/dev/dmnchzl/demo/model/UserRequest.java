package dev.dmnchzl.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {

    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String email;
    private String password;

    @Override
    public String toString() {
        return "UserRequest { firstName: '" + this.firstName +
            "', lastName: '" + this.lastName +
            "', birthDate: '" + this.birthDate +
            "', email: '" + this.email +
            "', password: '" + this.password + "' }";
    }

}
