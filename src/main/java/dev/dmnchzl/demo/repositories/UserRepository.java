package dev.dmnchzl.demo.repositories;

import dev.dmnchzl.demo.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<User, String> {

    Flux<User> findAllByFirstName(String firstName);

    Flux<User> findAllByLastName(String lastName);

    Mono<User> findByEmail(String email);

}
