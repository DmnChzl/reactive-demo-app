package dev.dmnchzl.demo.controllers;

import dev.dmnchzl.demo.model.AuthRequest;
import dev.dmnchzl.demo.model.AuthResponse;
import dev.dmnchzl.demo.services.UserService;
import dev.dmnchzl.demo.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class AuthController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @PostMapping("/auth")
    public Mono<ResponseEntity<AuthResponse>> authUser(@RequestBody AuthRequest authRequest) {
        return userService.findByEmail(authRequest.getUsername())
            .filter(user -> passwordEncoder.matches(authRequest.getPassword(), user.getPassword()))
            .map(user -> {
                AuthResponse authResponse = new AuthResponse(jwtUtil.generateToken(user));
                return ResponseEntity.status(HttpStatus.OK).body(authResponse);
            })
            .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }

}
