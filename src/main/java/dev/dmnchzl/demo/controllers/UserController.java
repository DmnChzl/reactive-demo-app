package dev.dmnchzl.demo.controllers;

import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserRequest;
import dev.dmnchzl.demo.model.UserResponse;
import dev.dmnchzl.demo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class UserController {

    private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(value = "/users")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserResponse> postUser(@RequestBody UserRequest userRequest) {
        LOGGER.debug(userRequest.toString());

        return userService.create(userRequest);
    }

    @GetMapping(value = "/users")
    // @ResponseStatus(HttpStatus.OK)
    public Flux<UserResponse> getAllUsers() {
        return userService.findAll();
    }

    @GetMapping(value = "/users/{id}")
    public Mono<ResponseEntity<UserDetailsResponse>> getUserById(@PathVariable("id") String id) {
        LOGGER.debug("ID: " + id); // System.out.println("ID: " + id);

        return userService.findById(id)
            .map(user -> ResponseEntity.status(HttpStatus.OK).body(user))
            .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PutMapping(value = "/users/{id}")
    public Mono<ResponseEntity<UserResponse>> getUserById(@PathVariable("id") String id, @RequestBody UserRequest userRequest) {
        return userService.updateById(id, userRequest)
                .map(user -> ResponseEntity.status(HttpStatus.OK).body(user))
                .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @DeleteMapping(value = "/users/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Mono<ResponseEntity<UserResponse>> delUserById(@PathVariable("id") String id) {
        return userService.deleteById(id)
            .map(user -> ResponseEntity.status(HttpStatus.OK).body(user))
            .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

}
