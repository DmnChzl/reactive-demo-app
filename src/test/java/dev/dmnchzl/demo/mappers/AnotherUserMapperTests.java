package dev.dmnchzl.demo.mappers;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserRequest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnotherUserMapperTests {

    @Test
    public void shouldMapRequestToUser() {
        UserRequest userRequest = new UserRequest("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        User user = AnotherUserMapperImpl.getInstance().toUserFromRequest(userRequest);

        assertEquals(user.getEmail(), userRequest.getEmail());
    }

}
