package dev.dmnchzl.demo.mappers;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserResponse;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserMapperTests {

    @Test
    public void shouldMapUserToResponse() {
        User user = new User("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        UserResponse userResponse = UserMapper.MAPPER.fromUserToResponse(user);

        assertEquals(userResponse.getFullName(), user.getFirstName() + " " + user.getLastName());
    }

    @Test
    public void shouldMapUserToDetails() {
        User user = new User("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        UserDetailsResponse userDetailsResponse = UserMapper.MAPPER.fromUserToDetails(user);

        assertEquals(userDetailsResponse.getEmail(), user.getEmail());
    }
}
