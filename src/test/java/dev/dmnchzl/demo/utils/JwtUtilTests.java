package dev.dmnchzl.demo.utils;

import dev.dmnchzl.demo.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(properties = {
    "jwt.secret=FdSkhVGza2vtt6ClFAtsEn3kx0lHFaRc4vK1fl+Im3xIXvnrrpK/1N2NdH52r2r+pualiF14vaQZ9rkFPAh0lA==",
    "jwt.expirationtime=3600000"
})
public class JwtUtilTests {

    @Autowired
    private JwtUtil jwtUtil;

    @Test
    public void shouldGetUsernameFromToken() {
        User user = new User.UserBuilder()
            .withFirstName("Jane")
            .withLastName("Doe")
            .withBirthDate(LocalDate.of(1991, 7, 27))
            .withEmail("jane.doe@pm.me")
            .withPassword("$2a$10$8pLwcYPzEIeknd.837a/jeY7fgKUxPodMNAd/gKf5ONUxKFypfY4e")
            .withUserRole()
            .build();

        String token = jwtUtil.generateToken(user);
        assertEquals(jwtUtil.getUsernameFromToken(token), "jane.doe@pm.me");
    }
}
