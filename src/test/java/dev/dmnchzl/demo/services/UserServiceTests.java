package dev.dmnchzl.demo.services;

import dev.dmnchzl.demo.model.User;
import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserRequest;
import dev.dmnchzl.demo.model.UserResponse;
import dev.dmnchzl.demo.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    public void shouldCreateUser() {
        User user = new User("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        user.setId("123");

        when(userRepository.save(any(User.class))).thenReturn(Mono.just(user));
        when(passwordEncoder.encode(any(String.class))).thenAnswer(it -> it.getArguments()[0]);

        UserRequest userRequest = new UserRequest("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");

        Mono<UserResponse> createUser = userService.create(userRequest);
        StepVerifier.create(createUser)
            .consumeNextWith(it -> {
                assertEquals(it.getFullName(), userRequest.getFirstName() + " " + userRequest.getLastName());
            })
            .verifyComplete();
    }

    @Test
    public void shouldGetAllUsers() {
        User johnDoe = new User.UserBuilder()
            .withFirstName("John")
            .withLastName("Doe")
            .withBirthDate(LocalDate.of(1993, 5, 21))
            .withEmail("john.doe@pm.me")
            .withPassword("azerty")
            .withAdminRole()
            .build();

        johnDoe.setId("123");

        User janeDoe = new User.UserBuilder()
            .withFirstName("Jane")
            .withLastName("Doe")
            .withBirthDate(LocalDate.of(1993, 5, 21))
            .withEmail("jane.doe@pm.me")
            .withPassword("qwerty")
            .withUserRole()
            .build();

        janeDoe.setId("456");

        when(userRepository.findAll()).thenReturn(Flux.just(johnDoe, janeDoe));

        Flux<UserResponse> findAll = userService.findAll();
        StepVerifier.create(findAll)
            .expectNextCount(2)
            .verifyComplete();
    }

    @Test
    public void shouldGetUserFromId() {
        User user = new User.UserBuilder()
            .withFirstName("John")
            .withLastName("Doe")
            .withBirthDate(LocalDate.of(1993, 5, 21))
            .withEmail("john.doe@pm.me")
            .withPassword("azerty")
            .withUserRole()
            .build();

        user.setId("123");

        when(userRepository.findById("123")).thenReturn(Mono.just(user));

        Mono<UserDetailsResponse> findById = userService.findById("123");
        StepVerifier.create(findById)
            .consumeNextWith(it -> {
                String fullName = it.getFirstName() + " " + it.getLastName();
                assertEquals(fullName, user.getFirstName() + " " + user.getLastName());
            })
            .verifyComplete();
    }

    @Test
    public void shouldGetUserFromEmail() {
        User user = new User.UserBuilder()
            .withFirstName("John")
            .withLastName("Doe")
            .withBirthDate(LocalDate.of(1993, 5, 21))
            .withEmail("john.doe@pm.me")
            .withPassword("azerty")
            .withUserRole()
            .build();

        user.setId("123");

        when(userRepository.findByEmail("john.doe@pm.me")).thenReturn(Mono.just(user));

        Mono<User> findByEmail = userService.findByEmail("john.doe@pm.me");
        StepVerifier.create(findByEmail)
            .consumeNextWith(it -> {
                String fullName = it.getFirstName() + " " + it.getLastName();
                assertEquals(fullName, user.getFirstName() + " " + user.getLastName());
            })
            .verifyComplete();
    }

    @Test
    public void shouldUpdateUserById() {
        User johnDoe = new User("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        johnDoe.setId("123");

        User janeDoe = new User("Jane", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        janeDoe.setId("123");

        when(userRepository.findById("123")).thenReturn(Mono.just(johnDoe));
        when(userRepository.save(any(User.class))).thenReturn(Mono.just(janeDoe));

        UserRequest userRequest = new UserRequest("Jane", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");

        Mono<UserResponse> updateById = userService.updateById("123", userRequest);
        StepVerifier.create(updateById)
            .consumeNextWith(it -> {
                assertEquals(it.getFullName(), userRequest.getFirstName() + " " + userRequest.getLastName());
            })
            .verifyComplete();
    }

    @Test
    public void shouldDeleteBy() {
        User user = new User("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");
        user.setId("123");

        when(userRepository.findById("123")).thenReturn(Mono.just(user));
        when(userRepository.deleteById("123")).thenReturn(Mono.empty());

        Mono<UserResponse> deleteById = userService.deleteById("123");
        StepVerifier.create(deleteById)
            .consumeNextWith(it -> {
                assertEquals(it.getFullName(), "John Doe");
            })
            .verifyComplete();
    }

}
