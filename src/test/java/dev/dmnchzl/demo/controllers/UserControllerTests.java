package dev.dmnchzl.demo.controllers;

import dev.dmnchzl.demo.model.UserDetailsResponse;
import dev.dmnchzl.demo.model.UserRequest;
import dev.dmnchzl.demo.model.UserResponse;
import dev.dmnchzl.demo.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = UserController.class, excludeAutoConfiguration = ReactiveSecurityAutoConfiguration.class)
public class UserControllerTests {

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private UserService userService;

    @Test
    public void shouldCreateNewUser() {
        UserResponse userResponse = new UserResponse("123", "John Doe", "john.doe@pm.me", 30);

        when(userService.create(any(UserRequest.class))).thenReturn(Mono.just(userResponse));

        UserRequest userRequest = new UserRequest("John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");

        webClient
            .method(HttpMethod.POST)
            .uri("/api/users")
            .bodyValue(userRequest)
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.CREATED)
            .expectBody(UserResponse.class);
    }

    @Test
    public void shouldGetAllUsers() {
        UserResponse rickSanchez = new UserResponse("123", "Rick Sanchez", "rick.sanchez@pm.me", 70);
        UserResponse mortySmith = new UserResponse("456", "Morty Smith", "morty.smith@pm.me", 14);

        when(userService.findAll()).thenReturn(Flux.just(rickSanchez, mortySmith));

        webClient
            .method(HttpMethod.GET)
            .uri("/api/users")
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.OK)
            .expectBody(UserResponse[].class);
    }

    @Test
    public void shouldGetUserById() {
        UserDetailsResponse userDetailsResponse = new UserDetailsResponse("123", "John", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");

        when(userService.findById("123")).thenReturn(Mono.just(userDetailsResponse));

        webClient
            .method(HttpMethod.GET)
            .uri("/api/users/123")
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.OK)
            .expectBody(UserDetailsResponse.class);
    }

    @Test
    public void shouldUpdateUserById() {
        UserResponse userResponse = new UserResponse("123", "Jane Doe", "john.doe@pm.me", 30);

        when(userService.updateById(eq("123"), any(UserRequest.class))).thenReturn(Mono.just(userResponse));

        UserRequest userRequest = new UserRequest("Jane", "Doe", LocalDate.of(1993, 5, 21), "john.doe@pm.me", "azerty");

        webClient
            .method(HttpMethod.PUT)
            .uri("/api/users/123")
            .bodyValue(userRequest)
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.OK)
            .expectBody(UserResponse.class);
    }

    @Test
    public void shouldDelUserById() {
        UserResponse userResponse = new UserResponse("123", "John Doe", "john.doe@pm.me", 30);

        when(userService.deleteById("123")).thenReturn(Mono.just(userResponse));

        webClient
            .method(HttpMethod.DELETE)
            .uri("/api/users/123")
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.OK)
            .expectBody(UserResponse.class);
    }

}
